function removeEverything() {
    while (document.body.firstChild) {
        document.body.firstChild.remove();
    }
}
function getRandomInt(min, max) {
  min = Math.ceil(min);
  max = Math.floor(max);
  return Math.floor(Math.random() * (max - min + 1)) + min; //The maximum is inclusive and the minimum is inclusive
}
function getText(){
  var textNum = getRandomInt(1, 2);
  return browser.extension.getURL("texts/" + textNum + ".html");
}

function makeRequest(method, url, callback){
	var request = new XMLHttpRequest();
	request.open(method, url, true);
	request.onload = function(){
		if(request.status >= 200 && request.status < 400){
      var response = this.response;
			// document.querySelector('div').innerHTML= this.response;
			callback(response);
		}
		else{
			// document.querySelector('div').innerHTML= this.response;
			console.warn("Error in dealing with request");
		}
	};
	request.onerror = function(){
		console.warn("Connection error");
	};
	request.send();
};
function renderHTML(data){
  document.body.innerHTML = data;
}


var textURL = getText();
removeEverything();

makeRequest("GET", textURL, renderHTML);
